﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;

namespace RevenueLoadPinger
{
    public class EmailHelper
    {
        // Methods
        public static void Send(string toAddress, string subject, string body)
        {
            MailMessage message = new MailMessage("reports@richrivermedia.com", toAddress, subject, body);
            new SmtpClient().Send(message);
        }

    }
}
