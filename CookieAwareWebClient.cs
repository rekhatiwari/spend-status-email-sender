﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace Utility
{
    public class CookieAwareWebClient : WebClient
    {
        // Fields
        private CookieContainer m_container = new CookieContainer();

        // Methods
        protected override WebRequest GetWebRequest(Uri address)
        {
            WebRequest request = base.GetWebRequest(address);
            if (!this.TimeOut.HasValue) this.TimeOut = 300000; // 5 min default
            request.Timeout = this.TimeOut.Value;

            if (request is HttpWebRequest)
            {
                (request as HttpWebRequest).CookieContainer = this.m_container;
            }
            return request;
        }

        // Properties
        public int? TimeOut { get; set; }
    }


}
