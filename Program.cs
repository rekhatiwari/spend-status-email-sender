﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;

namespace Utility
{
    class RevenueLoadPinger
    {
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();

            //SEND DAILY SPEND STATUS ON LAST PING
            try
            {
                using (CookieAwareWebClient c = new CookieAwareWebClient())
                {
                    //TimeSpan lastPingingTime = new TimeSpan(22, 59, 0); //Send mail on last ping only i.e. after 11.00 PM UTC
                    //TimeSpan curTime = DateTime.UtcNow.TimeOfDay;
                    //if (curTime > lastPingingTime)
                    //{
                        int timeout;
                        if (int.TryParse(ConfigurationManager.AppSettings["TimeoutInMilliseconds"], out timeout))
                            c.TimeOut = timeout;

                        Uri uri = new Uri(ConfigurationManager.AppSettings["SpendDailyStatusUrlToPing"]);
                        Console.WriteLine(string.Format("Pinging {0}", uri.AbsoluteUri));
                        sw.Start();
                        string result = c.DownloadString(uri);
                        sw.Stop();
                        Console.WriteLine(result);
                        Console.WriteLine(string.Format("Completed in {0} seconds", sw.Elapsed.TotalSeconds));
                    //}
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
